var express = require('express');
const axios = require('axios');
var router = express.Router();


const APP_ID = '5ffc44dafa533db488f4fdb3'

/*
* GET Users
*/
router.get('/users', function(req, res) { 
  
  
  res.set({ //preventing cors
    "Access-Control-Allow-Origin": "http://localhost:3000",
    'Access-Control-Allow-Credentials': true //setting credentials to true to allow cookie set on the frontend
  });  

  axios.get('https://dummyapi.io/data/api/user?limit=50', {
      headers: { 'app-id': APP_ID }
    })
    .then(data => {
      if(data.status === 200){ //200 status
        return data
      } else {
          throw new Error(`The HTTP status of the reponse: ${data.status} (${data.statusText})`);
      }
    })
    .then(data => {
        console.log(req.cookies.deletedProfiles)
        deletedProfiles = req.cookies.deletedProfiles;
        returnedProfiles = []
        if(deletedProfiles === undefined){
          deletedProfiles = []
        }
        data.data.data.forEach(user => {
          if(!deletedProfiles.includes(user.id)){
            returnedProfiles.push(user);
          }
        });
        console.log(deletedProfiles)
        res.json(returnedProfiles)
      }
    )
    .catch(err => res.send({msg: err}));

});

/*
* DELETE to delete a user
*/
router.delete('/users/:id', function(req, res) {

  console.log("Deleting")
  res.set({ //preventing cors
    "Access-Control-Allow-Origin": "http://localhost:3000",
    'Access-Control-Allow-Credentials': true
  });  

  var userid = req.params.id;

  try{
    var cookie = req.cookies.deletedProfiles;
    if (cookie === undefined) { //cookie defined for the first time
      arr = []
      arr.push(userid);
      res.cookie('deletedProfiles', arr);
    } else {
      arr = req.cookies.deletedProfiles
      arr.push(userid);
      res.cookie('deletedProfiles', arr);
    } 
    console.log(req.cookies.deletedProfiles)
    res.send({msg: ''});
  }
  catch (e){
    res.send({msg: e})
  }


});
  

/*
 * Handle preflighted request
 */
router.options("/*", function(req, res, next){
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.header('Access-Control-Allow-Methods', 'GET,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.header('Access-Control-Allow-Credentials', true);
  res.send(200);
});

module.exports = router;
