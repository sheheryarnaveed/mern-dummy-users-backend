# Setup
1. `git clone https://gitlab.com/sheheryarnaveed/mern-dummy-users-backend.git`
2. `cd` into the cloned directory
3. `npm install`
4. `node app.js` or use `nodemon app.js` if nodemon is installed globally 


## Note
- Backend is developed in NodeJS as express application
- Make sure to start the backend before running the frontend
- The backend runs on port 3001
- NodeJS version v14.4.0 was used
